#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/lineage_viva.mk

COMMON_LUNCH_CHOICES := \
    lineage_viva-user \
    lineage_viva-userdebug \
    lineage_viva-eng
