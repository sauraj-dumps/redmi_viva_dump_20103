## missi_phoneext4_eea-user 13 TP1A.220624.014 V14.0.1.0.TGDEUXM release-keys
- Manufacturer: xiaomi
- Platform: mt6781
- Codename: viva
- Brand: Redmi
- Flavor: missi_phoneext4_eea-user
- Release Version: 13
- Kernel Version: 4.14.186
- Id: TP1A.220624.014
- Incremental: V14.0.1.0.TGDEUXM
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: true
- Locale: en-GB
- Screen Density: undefined
- Fingerprint: Redmi/viva_eea/viva:12/SP1A.210812.016/V14.0.1.0.TGDEUXM:user/release-keys
- OTA version: 
- Branch: missi_phoneext4_eea-user-13-TP1A.220624.014-V14.0.1.0.TGDEUXM-release-keys
- Repo: redmi_viva_dump_20103
